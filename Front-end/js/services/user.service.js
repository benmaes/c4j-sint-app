angular.module('sintApp').factory('userService', function($http, restService, $q){
    var user = {
        name: ''
    };

    return {
        login : login,
        getUser : getUser
    };

    function getUser(name){
        var deferred = $q.defer();

        if(name !== user.name){
            restService.getData('home/' + name).then(function(response){
                user = response.data;
                deferred.resolve(user);
            });
        } else {
            deferred.resolve(user);
        }

        return deferred.promise;
    }

    function login(){
        var deferred = $q.defer();

        restService.getData('home').then(function(response){
            user = response.data;
            deferred.resolve(response.data);
        });

        return deferred.promise;
    }
});