angular.module('sintApp').factory('restService', function($http) {

    var path = 'http://192.168.2.108:8080/';

    return {
        getData : getData
    };

    function getData(url){
      return $http.get(path + url)
    }
});