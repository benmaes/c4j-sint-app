angular.module('sintApp').directive('userMenu', function () {
    return {
        restrict: 'E',
        templateUrl:'js/directives/user-menu/user-menu.template.html',
        controller: 'userMenuController',
        controllerAs: 'userMenu',
        scope :{
            user : '='
        },
        replace: false
    }
});