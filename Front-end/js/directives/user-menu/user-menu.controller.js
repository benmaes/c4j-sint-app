angular.module('sintApp').controller('userMenuController', function($rootScope){
    var vm = this;

    vm.login = login;
    vm.determineBlur = determineBlur;

    function login(){
        $rootScope.blur = true;
    }

    function determineBlur(){
        if(vm.user){
            $rootScope.blur = false;
        } else {
            $rootScope.blur = !$rootScope.blur;
        }
    }
});