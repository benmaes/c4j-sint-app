angular.module('sintApp').directive('menuBar', function () {
    return {
        restrict: 'E',
        templateUrl:'js/directives/menu-bar/menu-bar.template.html',
        controller: 'menuBarController',
        controllerAs: 'menu',
        scope :{},
        replace: true
    }
});