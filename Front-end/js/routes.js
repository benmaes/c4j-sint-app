angular.module('sintApp').config(['$routeProvider',
    function($routeProvider){
        $routeProvider
            .when('/', {
                templateUrl: 'js/pages/home-page/home-page.html'
            })
            .when('/editor', {
                templateUrl: 'js/pages/editor/editor.html',
                controller: 'editorController',
                controllerAs: 'editor'
            })
            .otherwise({
                redirectTo:'/'
            });

}]);

angular.module('sintApp').run(function($rootScope){
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
        $rootScope.home = next.templateUrl.indexOf('home-page') > -1;
    });
});

