angular.module('sintApp').controller('editorController', function($scope,userService, templateService){

    var vm = this;

    userService.login().then(function(user){
        vm.name = user.name;
    });

    templateService.getTextTemplates().then(function(textLabels){
        vm.textLabels = textLabels;
        $scope.textLabels = textLabels;
        console.log(vm.textLabels);
    });

    $scope.values = [{
        id: 1,
        name: "Template Image 1"
    }, {
        id: 2,
        name: "Template Image 2"
    },
    {
        id:3,
        name: "Template Image 3"
    },
    {
        id:4,
        name: "Template Image 4"
    },
    {
        id:5,
        name: "Template Image 5"
    }];
});