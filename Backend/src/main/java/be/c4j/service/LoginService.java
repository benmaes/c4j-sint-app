package be.c4j.service;

import be.c4j.model.User;

public interface LoginService {

    User getUserByName(String name, String password);

}
