package be.c4j.service;

import be.c4j.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class LoginServiceImpl implements LoginService{

    private static List<User> userList = new ArrayList<User>();


    @Override
    public User getUserByName(String username, String password) {
        for(User user : userList){
            if(username.equals(user.getName()) && password.equals(user.getPassword())){
                return user;
            }
        }

        return null;
    }

    @PostConstruct
    public void init(){
        User user1 = new User();

        user1.setId(1);
        user1.setName("Ben");
        user1.setPassword("Ben");

        userList.add(user1);
    }


}
