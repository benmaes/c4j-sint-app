package be.c4j.service;

import be.c4j.model.TextTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class TextTemplateServiceImpl implements TextTemplateService{

    private static List<TextTemplate> textTemplateList = new ArrayList<TextTemplate>();


    @Override
    public void createTextTemplate(TextTemplate textTemplate) {
        textTemplateList.add(textTemplate);
    }

    @Override
    public List<TextTemplate> retrieveAll() {
        return textTemplateList;
    }

    @PostConstruct
    public void init() {
        for(int i = 1; i<=5; i++) {
            TextTemplate template = new TextTemplate();
            template.setId(i);
            template.setLabel();
            template.setContent("This is template nr. " + i + "!");
            textTemplateList.add(template);
        }
    }
}
