package be.c4j.service;

import be.c4j.model.TextTemplate;

import be.c4j.model.TextTemplate;

import java.util.List;

public interface TextTemplateService {

    void createTextTemplate(TextTemplate text);
    List<TextTemplate> retrieveAll();

}
