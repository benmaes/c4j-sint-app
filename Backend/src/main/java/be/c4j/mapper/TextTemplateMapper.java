package be.c4j.mapper;

import be.c4j.model.TextTemplate;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class TextTemplateMapper {

    private TextTemplateMapper(){

    }


    public static TextTemplate mapJson(String json){

        JSONObject data = null;

        try {
            data = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextTemplate textTemplate = new TextTemplate();

        try {
            textTemplate.setLabel();
            textTemplate.setContent(data.get("text").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return textTemplate;
    }
}
