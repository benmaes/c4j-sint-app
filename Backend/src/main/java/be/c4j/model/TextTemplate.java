package be.c4j.model;

public class TextTemplate {

    private int id;
    private String label;
    private String content;
    private static int counter = 1;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel() {
        this.label = "Template " + counter;
        counter++;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
