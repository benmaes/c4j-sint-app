package be.c4j.controller;


import be.c4j.model.User;
import be.c4j.service.LoginService;
import com.sun.jersey.spi.resource.Singleton;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/home")
@Singleton
public class HomeController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByUser() {
        User user = new User();
        user.setName("Ben");

        return Response
                .status(200)
                .entity(user)
                .build();
    }

    @GET
    @Path("/{name}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(@PathParam("name")String name, @PathParam("password")String password) {
        User user = loginService().getUserByName(name, password);
        if(user != null) {
            return Response.status(200).entity(user).build();
        } else {
            return Response.status(201).entity("Login failed").build();
        }


    }

    public LoginService loginService(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-config.xml");
        return context.getBean(LoginService.class);
    }
}
