package be.c4j.controller;


import be.c4j.model.TextTemplate;
import be.c4j.mapper.TextTemplateMapper;
import be.c4j.service.TextTemplateService;
import com.sun.jersey.spi.resource.Singleton;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/text")
@Singleton
public class TextTemplateController {


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTextTemplate(String json){
        TextTemplate text = TextTemplateMapper.mapJson(json);
        textTemplateService().createTextTemplate(text);

        return Response.status(200)
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response retrieveAll() {
        List<TextTemplate> list = textTemplateService().retrieveAll();
        return Response.status(200)
                .entity(list).build();
    }


    public TextTemplateService textTemplateService(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-config.xml");
        return context.getBean(TextTemplateService.class);
    }


}
