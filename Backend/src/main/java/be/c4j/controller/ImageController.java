package be.c4j.controller;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Path("image")
public class ImageController {

    @Context
    private ServletContext context;


    @GET
    @Path("{imageName}")
    @Produces("image/jpeg")
    public Response getFullImage(@PathParam("imageName") String imageName) {

        InputStream image = context.getResourceAsStream("/WEB-INF/image/"+imageName);

        return createImageResponse(image);
    }

    private Response createImageResponse(final InputStream image) {

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        try {
            while ((nRead = image.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            buffer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Response.ok()
                .entity(buffer.toByteArray()).build();
    }




}
